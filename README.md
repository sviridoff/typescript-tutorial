# typescript-tutorial

* Typescript es un superset de Javascript, staticamente tipado, con utilidades para la organizacion de codigo.
* Sabe interpretar el contexto.
* Soporta la sintaxis es2015 - es2017
* Implement solo las features a partir de stage 3.

## Types:

# Boolean:

```javascript
let isDone: boolean = false;
```

# Number:

```
let decimal: number = 6;
let hex: number = 0xf00d;
```

# String:

```javascript
let color: string = 'blue';

let age: number = 37;
let sentence: string = \`Hello, my name is ${ fullName }\`
}`;
```

# Array:

```javascript
let list: number[] = [1, 2, 3];
let list: Array<number> = [1, 2, 3];
```

# Tuple:

```javascript
let x: [string, number];
x = ["hello", 10];
```

# Enum (friendly names to sets of numeric values):

```javascript
enum Color {Red, Green, Blue}
let c: Color = Color.Green;

enum Color {Red = 1, Green, Blue}
enum Color {Red = 1, Green = 2, Blue = 4}
```

# Any:

```javascript
let notSure: any = 4;
```

# Void:

```javascript
function warnUser(): void {
    console.log("This is my warning message");
}

(undefined or null)
let unusable: void = undefined;
```

# Type assertions:

```javascript
let someValue: any = "this is a string";
let strLength: number = (<string>someValue).length;
let strLength: number = (someValue as string).length;
```

# Lambda function:

```javascript
let foo: () => void;
foo = () => {};
```

# Return functions value:

```javascript
function warnUser(): string {
   return 'This is my warning message';
}
```

# Interface:

```javascript
interface IProps {
    name: string;    
}

Puede usarse extends y implements

interface IValidator {
    valide(): boolean;
}

interface IRepository extends IValidator {
    get(): string;
}

class Repository implements IRepository {
    get(): string {
        return 'object';
    }
    
    valide(): boolean {
        return true;
    }
}
```

# Type alias:

```javascript
type IState = {
    nama = string;    
}

Puede usarse |(or) y &(extend)

type TName = {
    name: string;
}

type TSurname = {
    surname: string; 
}

type TFullname = TName & TSurname;

const fullname: TFullname = {
    name: 'Alex',
    surname: 'Sviridov',
}
```

## Utilities

# Decorators

```javascript
function log(target, key, descriptor) {
    console.log(`${key} was called!`);
}

class P {
    @log
    foo() {
        console.log('Do something');
    }
}

const p = new P();
p.foo();
```

# Namespaces.
# Scope methods/variables: protected, public, private.
# Syntactic sugar: this.vars.
# Generic types.

# Example

```javascript
... Me fui a dormir

```


